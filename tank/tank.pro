#-------------------------------------------------
#
# Project created by QtCreator 2017-01-03T17:33:49
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = tank
TEMPLATE = app


SOURCES += main.cpp \
    tank.cpp \
    obstacle.cpp \
    tir.cpp \
    jeu.cpp \
    menujeu.cpp \
    jeuia.cpp

HEADERS  += \
    tank.h \
    define.h \
    obstacle.h \
    tir.h \
    jeu.h \
    menujeu.h \
    jeuia.h

FORMS    +=

DISTFILES +=

RESOURCES += \
    ressources.qrc
