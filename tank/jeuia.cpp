#include "jeuia.h"
#include "tank.h"
#include<QGraphicsEllipseItem>


Jeuia::Jeuia()
{
    scene = new QGraphicsScene();
    scene->setSceneRect(0,0,LARGEUR+250,LONGUEUR);
    init_terrain();
    init_player();
    init_interface();
    init_value();
    view = new QGraphicsView(scene);
    view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    view->setFixedSize(LARGEUR+250,LONGUEUR);
    view->show();
    focus();
}

void Jeuia::init_terrain()
{
    int random;
    int i=0;
    int x=0;
    int y=0;
    while(x<LARGEUR)
    {
        while(y<LONGUEUR)
        {
            terrain* t = new terrain(x,y);
            scene->addItem(t);
            y+=50;
        }
        y=0;
        x+=50;
    }
    srand(time(NULL));
    while(i <=100)
    {
        random=rand()%3;
        if(random==0)
        {
            arbre* a = new arbre(rand()%(LARGEUR-50),rand()%(LONGUEUR-50));
            scene->addItem(a);
            a->detecte();
        }
        else if(random==1)
        {
            roche* b = new roche(rand()%(LARGEUR-50),rand()%(LONGUEUR-50));
            scene->addItem(b);
            b->detecte();
        }
        else if(random==2)
        {
            eau* c = new eau(rand()%(LARGEUR-50),rand()%(LONGUEUR-50));
            scene->addItem(c);
            c->detecte();
        }
        i++;
    }

}

void Jeuia::init_player()
{
    srand(time(NULL));
    tour_player = 1;
    player1 = new Tank(rand()%(LARGEUR-50),rand()%(LONGUEUR-50),1);
    scene->addItem(player1);
    player1->detecteT();
    player1->setFlag(QGraphicsItem::ItemIsFocusable);

    player2 = new Tank(rand()%(LARGEUR-50),rand()%(LONGUEUR-50),2);
    scene->addItem(player2);
    player2->detecteT();
    player2->setFlag(QGraphicsItem::ItemIsFocusable);
}

void Jeuia::init_interface()
{
    //Création bouton choix Obus
    QGroupBox *radio = new QGroupBox ("Choisir le type d'obus");
    QRadioButton *tir1 = new QRadioButton("Obus de type 1");
    QRadioButton *tir2 = new QRadioButton("Obus de type 2");
    QRadioButton *tir3 = new QRadioButton("Obus de type 3");
    tir1->setChecked(true);
    QVBoxLayout *vbox = new QVBoxLayout;
    vbox->addWidget(tir1);
    vbox->addWidget(tir2);
    vbox->addWidget(tir3);
    radio->setLayout(vbox);
    QObject::connect(tir1, SIGNAL(clicked()),this,SLOT(choixobus1()));
    QObject::connect(tir1, SIGNAL(clicked()),this,SLOT(focus2()));
    QObject::connect(tir2, SIGNAL(clicked()),this,SLOT(choixobus2()));
    QObject::connect(tir2, SIGNAL(clicked()),this,SLOT(focus2()));
    QObject::connect(tir3, SIGNAL(clicked()),this,SLOT(choixobus3()));
    QObject::connect(tir3, SIGNAL(clicked()),this,SLOT(focus2()));

    //Création slider choix angle horinzontal
    QSlider *angleHorizontal = new QSlider(Qt::Horizontal);
    QLCDNumber *slider1 = new QLCDNumber();
    slider1->setSegmentStyle(QLCDNumber::Flat);
    QObject::connect(angleHorizontal, SIGNAL(valueChanged(int)), slider1, SLOT(display(int))) ;
    QObject::connect(angleHorizontal, SIGNAL(valueChanged(int)), this, SLOT(vangle(int))) ;
    QObject::connect(angleHorizontal, SIGNAL(valueChanged(int)), this, SLOT(changeAngle(int))) ;
    QObject::connect(angleHorizontal, SIGNAL(valueChanged(int)), this, SLOT(focus2()));

    //Création slider puissance tir
    QSlider *angleVertical = new QSlider(Qt::Vertical);
    QLCDNumber *slider2 = new QLCDNumber();
    slider2->setSegmentStyle(QLCDNumber::Flat);
    QObject::connect(angleVertical, SIGNAL(valueChanged(int)),slider2, SLOT(display(int))) ;
    QObject::connect(angleVertical, SIGNAL(valueChanged(int)),this, SLOT(vpuissance(int))) ;
    QObject::connect(angleVertical, SIGNAL(valueChanged(int)),this, SLOT(focus2())) ;

    //Création bouton de tir
    QPushButton *feu = new QPushButton ("Feu !");
    QObject::connect(feu, SIGNAL(clicked()),this,SLOT(tir())) ;
    QObject::connect(feu, SIGNAL(clicked()),this,SLOT(focus2())) ;

    //Création d'un bouton pour le focus et pour commencer le jeu
    QPushButton *focusb = new QPushButton ("Reprendre le focus");
    start = new QPushButton ("START");
    QObject::connect(start, SIGNAL(clicked()),this, SLOT(focus2())) ;
    QObject::connect(start,SIGNAL(clicked()),start,SLOT(hide()));
    QObject::connect(focusb, SIGNAL(clicked()),this, SLOT(focus2())) ;

    //ajout de l'inteface
    QGraphicsProxyWidget *proxy1 = scene->addWidget(radio);
    QGraphicsProxyWidget *proxy4 = scene->addWidget(angleHorizontal);
    QGraphicsProxyWidget *proxy5 = scene->addWidget(angleVertical);
    QGraphicsProxyWidget *proxy6 = scene->addWidget(slider1);
    QGraphicsProxyWidget *proxy7 = scene->addWidget(slider2);
    QGraphicsProxyWidget *proxy8 = scene->addWidget(feu);
    QGraphicsProxyWidget *proxy9 = scene->addWidget(start);
    QGraphicsProxyWidget *proxy10 = scene->addWidget(focusb);

    proxy1->setPos(LARGEUR+80, 10);
    proxy4->setPos(LARGEUR+30, 190);
    proxy5->setPos(LARGEUR+30, 250);
    proxy6->setPos(LARGEUR+100, 160);
    proxy7->setPos(LARGEUR+100, 280);
    proxy8->setPos(LARGEUR+100, 350);
    proxy9->setPos(LARGEUR/2, LONGUEUR/2);
    proxy10->setPos(LARGEUR+100, 410);

    angleHorizontal->setMaximum(359);
    angleHorizontal->setMinimum(0);
    angleHorizontal->setValue(0);
    angleVertical->setMaximum(90);
    angleVertical->setMinimum(10);
    angleVertical->setValue(10);
}

void Jeuia::init_value()
{
    obus=1;
    angle=0;
}

void Jeuia::focus()
{
    if(tour_player == 1)
    {
        player1->setFocus();

    }
    else
    {
        player2->setFocus();
    }
}

void Jeuia::ia()
{
    deplace();
    tiria();
}

void Jeuia::deplace()
{
    srand(time(NULL));
    int i;
    for (i=0;i<3;i++)
    {
        int alea;
        alea=rand()%4;
        if(alea==0)
                {
                    QKeyEvent * press = new QKeyEvent(QEvent::KeyPress,Qt::Key_Right, Qt::NoModifier);
                       player2->keyPressEvent(press);
                 }
                else if(alea==1)
                {
                    QKeyEvent * press2 = new QKeyEvent(QEvent::KeyPress,Qt::Key_Left, Qt::NoModifier);
                       player2->keyPressEvent(press2);
                }
                else if(alea==2)
                {
                    QKeyEvent * press3 = new QKeyEvent(QEvent::KeyPress,Qt::Key_Up, Qt::NoModifier);
                       player2->keyPressEvent(press3);
                 }
                else if(alea==3)
                {
                    QKeyEvent * press4 = new QKeyEvent(QEvent::KeyPress,Qt::Key_Down, Qt::NoModifier);
                       player2->keyPressEvent(press4);
                }
    }
}

void Jeuia::tiria()
{
    srand(time(NULL));
    int obus1=(rand()%3)+1;
    double theta =(rand()%70)+20 ;
    int puissance1=500*qSin(qDegreesToRadians(theta));
    if((obus1==2 && player2->get_obus2()>0) ||(obus1==3 && player2->get_obus3()>0) || obus1==1)
    {
        int angle = player1->getAngleTir();
        if(angle>=180)
        {
            angle-=180;
        }
        else if(angle<180)
        {
            angle+=180;
        }
        Tir * t = new Tir(angle,puissance1,obus1);
        t->setFlag(QGraphicsItem::ItemIsFocusable);
        t->setPos(player2->x()+25,player2->y()+25);
        scene->addItem(t);
        player2->clearFocus();
        player1->setFocus();
        tour_player=1;
        player2->maj_obus(obus);
    }else
    {
        tiria();
    }
}


Jeuia::~Jeuia()
{

}


void Jeuia::vangle(int vslider1)
{
    angle=vslider1;
}

void Jeuia::vpuissance(int vslider2)
{
    if(vslider2 <10)
    {
        vslider2 = 10;
    }
    double theta = vslider2;
    puissance=500*qSin(qDegreesToRadians(theta));
}

void Jeuia::choixobus1()
{
    obus=1;
}

void Jeuia::choixobus2()
{
    obus=2;
}

void Jeuia::choixobus3()
{
    obus=3;
}

void Jeuia::tir()
{
        if((obus==2 && player1->get_obus2()>0) ||(obus==3 && player1->get_obus3()>0) || obus==1)
        {
            Tir * t = new Tir(angle,puissance,obus);
            t->setFlag(QGraphicsItem::ItemIsFocusable);
            t->setPos(player1->x()+25,player1->y()+25);
            scene->addItem(t);
            player1->clearFocus();
            player2->setFocus();
            tour_player=2;
            player1->maj_obus(obus);
            ia();
    }
}

void Jeuia::focus2()
{
    if(tour_player == 1)
    {
        player1->setFocus();

    }
    else
    {
        player2->setFocus();
    }
}

void Jeuia::changeAngle(int angle)
{
    this->player1->setAngleTir(angle);
}
