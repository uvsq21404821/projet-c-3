#include "tir.h"
#include "obstacle.h"
#include "define.h"
#include "tank.h"
Tir::Tir(int a, int b, int m_obus)
{
    angle=a;
    puissance=b;
    obus=m_obus;
    vict=1;
    setZValue(6);
    switch (obus) {
    case 1:
        setPixmap(QPixmap(":/images/image/obus1.png").scaled(25,25,Qt::KeepAspectRatio));
        setRotation(a);
        break;
    case 2:
        setPixmap(QPixmap(":/images/image/obus2.png").scaled(25,25,Qt::KeepAspectRatio));
        setRotation(a);
        break;
    case 3:
        setPixmap(QPixmap(":/images/image/obus3.png").scaled(25,25,Qt::KeepAspectRatio));
        setRotation(a);
        break;
    default:
        break;
    }
    QTimer * timer = new QTimer();
    connect(timer,SIGNAL(timeout()),this,SLOT(move()));

    timer->start(2);
}

int Tir::impact()
{
    int res;
    if ( obus == 1)
   {
        crevasse1 * c = new crevasse1(x()-5,y()-5);
        scene()->addItem(c);

        res=c->destruction(obus);
   }
    else if ( obus == 2)
   {
        crevasse2 * c = new crevasse2(x()-15,y()-15);
        scene()->addItem(c);
        res=c->destruction(obus);
   }
    else if ( obus == 3)
   {
        crevasse3 * c = new crevasse3(x()-30,y()-30);
        scene()->addItem(c);
        res=c->destruction(obus);
   }
    return res;
}



void Tir::est_dans_ecran(int x, int y)
{
    if(x>=LARGEUR || x<=0 || y>= LONGUEUR || y<=0)
    {
        delete this;
    }
}

Tir::~Tir()
{

}

void Tir::move()
{
    if(puissance >0)
    {
        int STEP_SIZE = 5;
        double theta = angle;
        double dy = STEP_SIZE * qSin(qDegreesToRadians(theta));
        double dx = STEP_SIZE * qCos(qDegreesToRadians(theta));
        est_dans_ecran(x()+dx,y()+dy);
        setRotation(theta);
        setPos(x()+dx, y()+dy);
        puissance -=5;
    }
    else if (this->impact()==1 && vict==1)
    {
            delete this;
    }
    else
    {
            vict=2;
            QTimer * timer2 = new QTimer();
            connect(timer2,SIGNAL(timeout()),this,SLOT(victoire()));
            timer2->start(2000);
    }
}

void Tir::victoire()
{
    vict=2;
    exit(0);
}

