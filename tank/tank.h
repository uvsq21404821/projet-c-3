#ifndef TANK_H
#define TANK_H

#include "define.h"
#include <QGraphicsItem>
#include <QGraphicsPixmapItem>
#include <QKeyEvent>
#include <QList>
#include <typeinfo>

class Tank: public QGraphicsPixmapItem
{
private:
   int deplacement;
   int obus2;
   int obus3;
   int angle_tir_prec;
public:
   int num_joueur;
    Tank(int x,int y,int joueur);
    void keyPressEvent(QKeyEvent * event);
    void bloque(int tmpx,int tmpy);
    void detecteT();
    bool deplace();
    void deplace_crevase();
    int get_obus2();
    int get_obus3();
    void maj_obus(int obus);
    ~Tank();
    int getDeplacement() const;
    void setDeplacement(int value);
    int getAngleTir();
    void setAngleTir(int angle);
};

#endif // TANK_H
