#ifndef MENUJEU_H
#define MENUJEU_H
#include "define.h"
#include <QApplication>
#include <QGraphicsScene>
#include <QWidget>
#include <QObject>
#include <QLayout>
#include <QPushButton>
#include <QGraphicsView>
#include <QGraphicsProxyWidget>
#include <QVBoxLayout>
#include "define.h"
#include"jeu.h"
#include"jeuia.h"

class Menujeu: public QWidget
{
Q_OBJECT
public:
    Menujeu();
public slots:
    void jeuvs();
    void jeuia();
private:
QWidget fenetre;
QVBoxLayout *layout;
};

#endif // MENUJEU_H
