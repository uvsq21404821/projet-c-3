#include "Tank.h"
#include "define.h"
#include "obstacle.h"
#include "tir.h"

Tank::~Tank()
{

}

int Tank::getDeplacement() const
{
    return deplacement;
}

void Tank::setDeplacement(int value)
{
    deplacement = value;
}

int Tank::getAngleTir()
{
    return angle_tir_prec;
}

void Tank::setAngleTir(int angle)
{
     angle_tir_prec=angle;
}

Tank::Tank(int x, int y, int joueur)
{
    if(joueur==1){
        setPixmap(QPixmap(":/images/image/tankright.png").scaled(50,50,Qt::KeepAspectRatio));
    }
    else
    {
        setPixmap(QPixmap(":/images/image/tank2l.png").scaled(50,50,Qt::KeepAspectRatio));
    }
    deplacement= LARGEUR/10;
    obus2=10;
    obus3=5;
    num_joueur=joueur;
    setPos(x,y);
    setZValue(5);

}
void Tank::keyPressEvent(QKeyEvent * event)
{
    if(deplace())
    {
    int tmpx = x();
    int tmpy = y();

    if (event->key() == Qt::Key_Left)
    {
        if(pos().x() >0)
        {
            this->deplacement-=1;
            setPos(x()-20,y());
            if(num_joueur==1)
            setPixmap(QPixmap(":/images/image/tankleft.png").scaled(50,50,Qt::KeepAspectRatio));
            else
            setPixmap(QPixmap(":/images/image/tank2l.png").scaled(50,50,Qt::KeepAspectRatio));

        }
        bloque(tmpx,tmpy);
        deplace_crevase();
    }
    else if (event->key() == Qt::Key_Right)
    {
        if(pos().x()+50 <LARGEUR)
        {
            this->deplacement-=1;
            setPos(x()+20,y());
            if(num_joueur==1)
            setPixmap(QPixmap(":/images/image/tankright.png").scaled(50,50,Qt::KeepAspectRatio));
            else
            setPixmap(QPixmap(":/images/image/tank2r.png").scaled(50,50,Qt::KeepAspectRatio));
        }
        bloque(tmpx,tmpy);
        deplace_crevase();
    }
    else if (event->key() == Qt::Key_Up)
    {
         if(pos().y() >0)
         {
             this->deplacement-=1;
             setPos(x(),y()-20);
             if(num_joueur==1)
             setPixmap(QPixmap(":/images/image/tankup.png").scaled(50,50,Qt::KeepAspectRatio));
             else
             setPixmap(QPixmap(":/images/image/tank2u.png").scaled(50,50,Qt::KeepAspectRatio));
         }
         bloque(tmpx,tmpy);
         deplace_crevase();
    }
    else if (event->key() == Qt::Key_Down)
    {
         if(pos().y()+50 <LONGUEUR)
         {
             this->deplacement-=1;
             setPos(x(),y()+20);
             if(num_joueur==1)
             setPixmap(QPixmap(":/images/image/tankdown.png").scaled(50,50,Qt::KeepAspectRatio));
             else
             setPixmap(QPixmap(":/images/image/tank2d.png").scaled(50,50,Qt::KeepAspectRatio));
         }
         bloque(tmpx,tmpy);
         deplace_crevase();
    }
    }
}

void Tank::bloque(int tmpx, int tmpy)
{
    QList<QGraphicsItem *> obj = collidingItems();
    for(int i=0, n = obj.size();i<n;i++)
    {
        if(typeid(*(obj[i])) == typeid(arbre)  || typeid(*(obj[i])) == typeid(eau) ||typeid(*(obj[i])) == typeid(roche) || typeid(*(obj[i])) == typeid(Tank) )
        {
            setPos(tmpx,tmpy);
            deplacement+=1;
            return;
        }
    }
}

void Tank::detecteT()
{
        QList<QGraphicsItem *> obj = collidingItems();
        for(int i=0, n = obj.size();i<n;i++)
        {
            if(typeid(*(obj[i])) == typeid(arbre)  || typeid(*(obj[i])) == typeid(eau) ||typeid(*(obj[i])) == typeid(roche) ||typeid(*(obj[i])) == typeid(Tank) )
            {
                delete(obj[i]);
            }
        }
}

bool Tank::deplace()
{
    if(this->deplacement > 0)
    {
        return true;
    }
    return false;
}

void Tank::deplace_crevase()
{
    QList<QGraphicsItem *> obj = collidingItems();
       if(typeid(*(obj[0]))== typeid(crevasse1) || typeid(*(obj[0]))== typeid(crevasse2) || typeid(*(obj[0]))== typeid(crevasse3))
       {
           deplacement-=1;
       }
}

int Tank::get_obus2()
{
    return obus2;
}

int Tank::get_obus3()
{
    return obus3;
}

void Tank::maj_obus(int obus)
{
    if(obus==2)
    {
        obus2-=1;
    }
    else if(obus==3)
    {
        obus3-=1;
    }
}
