/****************************************************************************
** Meta object code from reading C++ file 'jeu.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../jeu.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'jeu.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Jeu_t {
    QByteArrayData data[11];
    char stringdata0[85];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Jeu_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Jeu_t qt_meta_stringdata_Jeu = {
    {
QT_MOC_LITERAL(0, 0, 3), // "Jeu"
QT_MOC_LITERAL(1, 4, 6), // "vangle"
QT_MOC_LITERAL(2, 11, 0), // ""
QT_MOC_LITERAL(3, 12, 8), // "vslider1"
QT_MOC_LITERAL(4, 21, 10), // "vpuissance"
QT_MOC_LITERAL(5, 32, 8), // "vslider2"
QT_MOC_LITERAL(6, 41, 10), // "choixobus1"
QT_MOC_LITERAL(7, 52, 10), // "choixobus2"
QT_MOC_LITERAL(8, 63, 10), // "choixobus3"
QT_MOC_LITERAL(9, 74, 3), // "tir"
QT_MOC_LITERAL(10, 78, 6) // "focus2"

    },
    "Jeu\0vangle\0\0vslider1\0vpuissance\0"
    "vslider2\0choixobus1\0choixobus2\0"
    "choixobus3\0tir\0focus2"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Jeu[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   49,    2, 0x0a /* Public */,
       4,    1,   52,    2, 0x0a /* Public */,
       6,    0,   55,    2, 0x0a /* Public */,
       7,    0,   56,    2, 0x0a /* Public */,
       8,    0,   57,    2, 0x0a /* Public */,
       9,    0,   58,    2, 0x0a /* Public */,
      10,    0,   59,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void Jeu::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Jeu *_t = static_cast<Jeu *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->vangle((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->vpuissance((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->choixobus1(); break;
        case 3: _t->choixobus2(); break;
        case 4: _t->choixobus3(); break;
        case 5: _t->tir(); break;
        case 6: _t->focus2(); break;
        default: ;
        }
    }
}

const QMetaObject Jeu::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_Jeu.data,
      qt_meta_data_Jeu,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Jeu::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Jeu::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Jeu.stringdata0))
        return static_cast<void*>(const_cast< Jeu*>(this));
    return QObject::qt_metacast(_clname);
}

int Jeu::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 7;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
