#include "jeu.h"
#include "tank.h"
#include<QGraphicsEllipseItem>


Jeu::Jeu()
{
    scene = new QGraphicsScene();
    scene->setSceneRect(0,0,LARGEUR+250,LONGUEUR);
    init_terrain();
    init_player();
    init_interface();
    init_value();
    view = new QGraphicsView(scene);
    view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    view->setFixedSize(LARGEUR+250,LONGUEUR);
    view->show();
    focus();
}

void Jeu::init_terrain()
{
    int random;
    int i=0;
    int x=0;
    int y=0;
    while(x<LARGEUR)
    {
        while(y<LONGUEUR)
        {
            terrain* t = new terrain(x,y);
            scene->addItem(t);
            y+=50;
        }
        y=0;
        x+=50;
    }
    srand(time(NULL));
    while(i <=100)
    {
        random=rand()%3;
        if(random==0)
        {
            arbre* a = new arbre(rand()%(LARGEUR-50),rand()%(LONGUEUR-50));
            scene->addItem(a);
            a->detecte();
        }
        else if(random==1)
        {
            roche* b = new roche(rand()%(LARGEUR-50),rand()%(LONGUEUR-50));
            scene->addItem(b);
            b->detecte();
        }
        else if(random==2)
        {
            eau* c = new eau(rand()%(LARGEUR-50),rand()%(LONGUEUR-50));
            scene->addItem(c);
            c->detecte();
        }
        i++;
    }

}

void Jeu::init_player()
{
    srand(time(NULL));
    tour_player = rand()%2+1;
    player1 = new Tank(rand()%(LARGEUR-50),rand()%(LONGUEUR-50),1);
    scene->addItem(player1);
    player1->detecteT();
    player1->setFlag(QGraphicsItem::ItemIsFocusable);

    player2 = new Tank(rand()%(LARGEUR-50),rand()%(LONGUEUR-50),2);
    scene->addItem(player2);
    player2->detecteT();
    player2->setFlag(QGraphicsItem::ItemIsFocusable);
}

void Jeu::init_interface()
{
    //Création bouton choix Obus
    QGroupBox *radio = new QGroupBox ("Choisir le type d'obus");
    QRadioButton *tir1 = new QRadioButton("Obus de type 1");
    QRadioButton *tir2 = new QRadioButton("Obus de type 2");
    QRadioButton *tir3 = new QRadioButton("Obus de type 3");
    tir1->setChecked(true);
    QVBoxLayout *vbox = new QVBoxLayout;
    vbox->addWidget(tir1);
    vbox->addWidget(tir2);
    vbox->addWidget(tir3);
    radio->setLayout(vbox);
    QObject::connect(tir1, SIGNAL(clicked()),this,SLOT(choixobus1()));
    QObject::connect(tir1, SIGNAL(clicked()),this,SLOT(focus2()));
    QObject::connect(tir2, SIGNAL(clicked()),this,SLOT(choixobus2()));
    QObject::connect(tir2, SIGNAL(clicked()),this,SLOT(focus2()));
    QObject::connect(tir3, SIGNAL(clicked()),this,SLOT(choixobus3()));
    QObject::connect(tir3, SIGNAL(clicked()),this,SLOT(focus2()));

    //Création slider choix angle horinzontal
    QSlider *angleHorizontal = new QSlider(Qt::Horizontal);
    QLCDNumber *slider1 = new QLCDNumber();
    slider1->setSegmentStyle(QLCDNumber::Flat);
    QObject::connect(angleHorizontal, SIGNAL(valueChanged(int)), slider1, SLOT(display(int))) ;
    QObject::connect(angleHorizontal, SIGNAL(valueChanged(int)), this, SLOT(vangle(int))) ;
    QObject::connect(angleHorizontal, SIGNAL(valueChanged(int)), this, SLOT(focus2()));

    //Création slider puissance tir
    QSlider *angleVertical = new QSlider(Qt::Vertical);
    QLCDNumber *slider2 = new QLCDNumber();
    slider2->setSegmentStyle(QLCDNumber::Flat);
    QObject::connect(angleVertical, SIGNAL(valueChanged(int)),slider2, SLOT(display(int))) ;
    QObject::connect(angleVertical, SIGNAL(valueChanged(int)),this, SLOT(vpuissance(int))) ;
    QObject::connect(angleVertical, SIGNAL(valueChanged(int)),this, SLOT(focus2())) ;

    //Création bouton de tir
    QPushButton *feu = new QPushButton ("Feu !");
    QObject::connect(feu, SIGNAL(clicked()),this,SLOT(tir())) ;
    QObject::connect(feu, SIGNAL(clicked()),this,SLOT(focus2())) ;

    //Création d'un bouton pour le focus et pour commencer le jeu
    QPushButton *focusb = new QPushButton ("Reprendre le focus");
    start = new QPushButton ("START");
    QObject::connect(start, SIGNAL(clicked()),this, SLOT(focus2())) ;
    QObject::connect(start,SIGNAL(clicked()),start,SLOT(hide()));
    QObject::connect(focusb, SIGNAL(clicked()),this, SLOT(focus2())) ;

    //ajout de l'inteface
    QGraphicsProxyWidget *proxy1 = scene->addWidget(radio);
    QGraphicsProxyWidget *proxy4 = scene->addWidget(angleHorizontal);
    QGraphicsProxyWidget *proxy5 = scene->addWidget(angleVertical);
    QGraphicsProxyWidget *proxy6 = scene->addWidget(slider1);
    QGraphicsProxyWidget *proxy7 = scene->addWidget(slider2);
    QGraphicsProxyWidget *proxy8 = scene->addWidget(feu);
    QGraphicsProxyWidget *proxy9 = scene->addWidget(start);
    QGraphicsProxyWidget *proxy10 = scene->addWidget(focusb);

    proxy1->setPos(LARGEUR+80, 10);
    proxy4->setPos(LARGEUR+30, 190);
    proxy5->setPos(LARGEUR+30, 250);
    proxy6->setPos(LARGEUR+100, 160);
    proxy7->setPos(LARGEUR+100, 280);
    proxy8->setPos(LARGEUR+100, 350);
    proxy9->setPos(LARGEUR/2, LONGUEUR/2);
    proxy10->setPos(LARGEUR+100, 410);

    angleHorizontal->setMaximum(359);
    angleHorizontal->setMinimum(0);
    angleHorizontal->setValue(0);
    angleVertical->setMaximum(90);
    angleVertical->setMinimum(10);
    angleVertical->setValue(10);
}

void Jeu::init_value()
{
    obus=1;
    angle=0;
}

void Jeu::focus()
{
    if(tour_player == 1)
    {
        player1->setFocus();

    }
    else
    {
        player2->setFocus();
    }
}

Jeu::~Jeu()
{

}


void Jeu::vangle(int vslider1)
{
    angle=vslider1;
}

void Jeu::vpuissance(int vslider2)
{
    if(vslider2 <10)
    {
        vslider2 = 10;
    }
    double theta = vslider2;
    puissance=500*qSin(qDegreesToRadians(theta));
}

void Jeu::choixobus1()
{
    obus=1;
}

void Jeu::choixobus2()
{
    obus=2;
}

void Jeu::choixobus3()
{
    obus=3;
}

void Jeu::tir()
{
    if(tour_player==1){
        if((obus==2 && player1->get_obus2()>0) ||(obus==3 && player1->get_obus3()>0) || obus==1)
        {
            Tir * t = new Tir(angle,puissance,obus);

            t->setFlag(QGraphicsItem::ItemIsFocusable);
            t->setPos(player1->x()+25,player1->y()+25);
            scene->addItem(t);
            player1->clearFocus();
            player2->setFocus();
            tour_player=2;
            player1->maj_obus(obus);
        }
    }
    else{
        if((obus==2 && player2->get_obus2()>0) ||(obus==3 && player2->get_obus3()>0)||obus==1)
        {
            Tir * t = new Tir(angle,puissance,obus);
            t->setPos(player2->x()+25,player2->y()+25);
            scene->addItem(t);
            player2->clearFocus();
            player1->setFocus();
            tour_player=1;
            player2->maj_obus(obus);
        }
    }
}

void Jeu::focus2()
{
    if(tour_player == 1)
    {
        player1->setFocus();

    }
    else
    {
        player2->setFocus();
    }
}
