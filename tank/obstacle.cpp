#include"obstacle.h"
#include"tank.h"
obstacle::obstacle()
{
}

bool obstacle::setResistance(int resist)
{
    if (resist == 1)
    {
        resistance=resistance-2;
    }
    else if ( resist == 2)
    {
        resistance=resistance-5;
    }
    else if ( resist == 3)
    {
        resistance=resistance-9;
    }
    if (resistance <= 0)
    {
        return false;
    }
    else
    {
        return true;
    }
}

void obstacle::detecte()
{
    QList<QGraphicsItem *> obj = collidingItems();
    for(int i=0,n=obj.size();i<n;i++)
    {
        if(typeid(*(obj[i])) == typeid(arbre)  || typeid(*(obj[i])) == typeid(eau) ||typeid(*(obj[i])) == typeid(roche) ||typeid(*(obj[i])) == typeid(Tank) )
        {
            delete(obj[i]);
        }
    }
}

int obstacle::destruction(int obus)
{

    QList<QGraphicsItem *> obj = collidingItems();
    for(int i=0,n=obj.size();i<n;i++)
    {
       if(typeid(*(obj[i]))== typeid(Tank))
       {
           Tank *t =dynamic_cast<Tank*>(obj[i]);
           if(t->num_joueur==1)
           {
                QGraphicsTextItem * vic = new QGraphicsTextItem(QString("Tank 2 gagne"));
                vic->setPos(LARGEUR/2-250,LONGUEUR/2-100);
                vic->setDefaultTextColor(QColor(255,0,0,200));
                vic->setFont(QFont("comic sans",50));
                scene()->addItem(vic);
                delete(t);

           }else if(t->num_joueur==2)
           {
               QGraphicsTextItem * vic = new QGraphicsTextItem(QString("Tank 1 gagne"));
               vic->setPos(LARGEUR/2-250,LONGUEUR/2-100);
               vic->setDefaultTextColor(QColor(0,0,255,200));
               vic->setFont(QFont("comic sans",50));
               scene()->addItem(vic);
               delete(t);
           }
           return 2;
       }
        else if(typeid(*(obj[i]))== typeid(arbre))
       {
           if ( obus == 1)
           {
                delete(obj[i]);
           }
           else if ( obus == 2)
           {
                delete(obj[i]);
           }
           else if ( obus == 3)
           {
                delete(obj[i]);
           }
       }
       else if(typeid(*(obj[i]))== typeid(roche))
       {
            roche *r =dynamic_cast<roche*>(obj[i]);
            if (! r->setResistance(obus))
            {
                if ( obus == 1)
               {
                    delete(obj[i]);
               }
                else if ( obus == 2)
               {
                    delete(obj[i]);
               }
                else if ( obus == 3)
               {
                    delete(obj[i]);
               }
            }else{delete(this);}
       }/*else if(typeid(*(obj[i]))== typeid(eau))
       {
          delete(obj[i]);
       }*/
    }
       return 1;
}


obstacle::~obstacle()
{

}
arbre::arbre(int x,int y)
{
    setPos(x,y);
    setPixmap(QPixmap(":/images/image/arbre.png").scaled(50,50,Qt::KeepAspectRatio));
    resistance = 1;
    bloquant = 1;
}

arbre::~arbre()
{
}
crevasse1::crevasse1(int x,int y)
{
    setPos(x,y);
    setPixmap(QPixmap(":/images/image/c.png").scaled(30,30,Qt::KeepAspectRatio));
    resistance = -2;
    bloquant = 0;
    setZValue(1);
}

crevasse1::~crevasse1()
{
}
crevasse2::crevasse2(int x,int y)
{
    setPos(x,y);
    setPixmap(QPixmap(":/images/image/c.png").scaled(50,50,Qt::KeepAspectRatio));
    resistance = -2;
    bloquant = 0;
    setZValue(2);
}

crevasse2::~crevasse2()
{
}
crevasse3::crevasse3(int x,int y)
{
    setPos(x,y);
    setPixmap(QPixmap(":/images/image/c.png").scaled(70,70,Qt::KeepAspectRatio));
    resistance = -2;
    bloquant = 0;
    setZValue(3);
}

crevasse3::~crevasse3()
{
}
eau::eau(int x,int y)
{
    setPos(x,y);
    setPixmap(QPixmap(":/images/image/eau.png").scaled(50,50,Qt::KeepAspectRatio));
    resistance = NMAX;
    bloquant = 1;
    setZValue(4);
}

eau::~eau()
{

}
roche::roche(int x,int y)
{
    setPos(x,y);
    setPixmap(QPixmap(":/images/image/roche.png").scaled(50,50,Qt::KeepAspectRatio));
    resistance = 7;
    bloquant = 1;
}

roche::~roche()
{

}
terrain::terrain(int x,int y)
{
    setPos(x,y);
    setPixmap(QPixmap(":/images/image/terrain.png"));
    resistance = 0;
    bloquant = 0;
}

terrain::~terrain()
{

}
