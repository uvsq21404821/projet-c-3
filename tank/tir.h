#ifndef TIR_H
#define TIR_H

#include <QGraphicsRectItem>
#include <QObject>
#include <QTimer>
#include <qmath.h>
#include <QList>
#include <typeinfo>
#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QGraphicsPixmapItem>
#include <QDebug>

class Tir: public QObject,public QGraphicsPixmapItem{
Q_OBJECT
public:
    Tir(int a,int b,int m_obus);
    int angle;
    int puissance;
    int obus;
    int impact();
    int vict;
    void est_dans_ecran(int x,int y);
    ~Tir();
public slots:
    void move();
    void victoire();
};
#endif // TIR_H
