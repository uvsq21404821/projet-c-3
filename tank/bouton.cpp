#include "bouton.h"

Bouton::Bouton()
{
    tir1 = new QPushButton("Obus de type 1",this);
    tir2 = new QPushButton("Obus de type 2",this);
    tir3 = new QPushButton("Obus de type 3",this);
    angleHorizontal = new QSlider(Qt::Horizontal,this);
    slider1 = new QLCDNumber(this);
    slider1->setSegmentStyle(QLCDNumber::Flat);
    QObject::connect(angleVertical, SIGNAL(valueChanged(int)), slider1, SLOT(display(int))) ;
    angleVertical = new QSlider(Qt::Vertical,this);
    angleHorizontal->setMaximum(359);
    angleVertical->setMaximum(89);
}
