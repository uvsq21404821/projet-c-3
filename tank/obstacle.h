#ifndef OBSTACLE_H
#define OBSTACLE_H

#include <QGraphicsItem>
#include <QGraphicsPixmapItem>
#include <QList>
#include <typeinfo>
#include"define.h"
#include<QGraphicsTextItem>
#include<QFont>
#include <QGraphicsItem>

#include <QGraphicsRectItem>
#include <QObject>
#include <typeinfo>
#include <QGraphicsScene>

class obstacle: public QGraphicsPixmapItem
{
public:
    int resistance; //resistance de l'objet si egal à 0 alors destruction
    int bloquant; //1 si empeche le deplacement du tank, 0 sinon
    obstacle();
    bool setResistance(int resist);
    void detecte();
    int destruction(int obus);
    virtual ~obstacle();
};

class arbre:public obstacle
{
public:
    arbre(int x,int y);
    virtual ~arbre();
};

class crevasse1:public obstacle
{
public:
    crevasse1(int x,int y);
    virtual ~crevasse1();
};
class crevasse2:public obstacle
{
public:
    crevasse2(int x,int y);
    virtual ~crevasse2();
};
class crevasse3:public obstacle
{
public:
    crevasse3(int x,int y);
    virtual ~crevasse3();
};

class eau:public obstacle
{
public:
    eau(int x,int y);
    virtual ~eau();
};

class roche:public obstacle
{
public:
    roche(int x,int y);
    virtual ~roche();
};
class terrain:public obstacle
{
public:
    terrain(int x,int y);
    virtual ~terrain();
};

#endif // OBSTACLE_H
