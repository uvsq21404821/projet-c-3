#ifndef JEUIA_H
#define JEUIA_H
#include "define.h"
#include <QApplication>
#include <QGraphicsScene>
#include "obstacle.h"
#include <QGraphicsRectItem>
#include <QWidget>
#include <QObject>
#include <QPushButton>
#include <QGraphicsView>
#include <QGraphicsProxyWidget>
#include <QGraphicsPixmapItem>
#include <QFont>
#include <QBrush>
#include <QLCDNumber>
#include <QRadioButton>
#include <QSlider>
#include <QKeyEvent>
#include <QWindow>
#include <QImage>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include "define.h"
#include "tank.h"
#include "tir.h"
#include <QList>
#include <QGroupBox>
#include <QVBoxLayout>
#include <qmath.h>
#include<Qtimer>

class Jeuia : public QObject
{
Q_OBJECT
public:
    Jeuia();
    QGraphicsScene *scene;
    QGraphicsView *view;
    int angle;
    int puissance;
    int obus;
    int tour_player;
    void init_terrain();
    void init_player();
    void init_interface();
    void init_value();
    Tank *player1;
    Tank *player2;
    QPushButton *start;
    void focus();
    void ia();
    void deplace();
    void tiria();
    ~Jeuia();


public slots:
    void vangle(int vslider1);
    void vpuissance(int vslider2);
    void choixobus1();
    void choixobus2();
    void choixobus3();
    void tir();
    void focus2();
    void changeAngle(int angle);



};

#endif // JEUIA_H
