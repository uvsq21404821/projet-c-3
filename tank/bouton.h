#ifndef BOUTON_H
#define BOUTON_H

#include <QApplication>
#include <QWidget>
#include <QPushButton>
#include <QLCDNumber>
#include <QSlider>
#include <QGraphicsProxyWidget>

class Bouton : public QWidget
{
    public:
    Bouton();
    QPushButton *tir1;
    QPushButton *tir2;
    QPushButton *tir3;
    QLCDNumber *slider1;
    QSlider *angleHorizontal;
    QLCDNumber *slider2;
    QSlider *angleVertical;
};

#endif // BOUTON_H
